image processing benchmark for several interactive programming language and library


-  matlab (image processing toolbox)
-  octave (image)
-  python (scipy,PIL)
-  julia (Images, GPUArrays, CuArrays, ArrayFire)
-   gmic
-   c++ (CImg)

What operations are tested
==========================
The benchmark tests at the moment 2 operations:
- image convolution by a small 5x5 kernel
- DFT of multi-dimensional arrays

Tests are reporting the average computation time over 50 runs.

How to install the environement
===============================

Matlab
--------
Get matlab from Mathworks and the image processing toolbox

Octave
------
`sudo apt-get install octave`
Then in octave:
`pkg install image`

Julia
------

Installing arrayfire for julia:

`wget https://arrayfire.s3.amazonaws.com/3.6.2/ArrayFire-v3.6.2_Linux_x86_64.sh`

`./Arrayfire_*_Linux_x86_64.sh --include-subdir --prefix=/opt`

`sudo sh -c "echo /opt/arrayfire/lib64 > /etc/ld.so.conf.d/arrayfire.conf"`

`sudo ldconfig`

Then in julia:
`] add ArrayFire`


CImg
-----
Get the library from the github repository:

`wget https://framagit.org/dtschump/CImg/raw/master/CImg.h`

GMIC
-----

Install the latest GMIC using:

`wget https://gmic.eu/files/linux/gmic_ubuntu_$(lsb_release -c | cut -f 2)_amd64.deb`

`dpkg --install gmic_ubuntu_$(lsb_release -c | cut -f 2)_amd64.deb`


How to run the tests
====================
Run the run.sh bash script to test all the benchmarks.

Result
=======
Intel(R) Xeon(R) CPU E5-1650 v3 @ 3.50GHz number of cores:12 + GeForce GTX 1080

| env | test | time (ms) |
|-----|------|-----------|
| matlab|convolve 5x5|168.37|
| matlab|convolve 3x3|121.37|
| matlab|fft 2048|33.33|
| octave|convolve 5x5|426.12|
| octave|convolve 3x3|307.21|
| octave|fft 2048|96.03|
| scipy |convolve 5x5| 538.0|
| pil   |convolve 5x5| 365.0|
| numpy  |fft 2048| 225.0|
| gmic  |convolve 5x5|71|
| gmic  |convolve 3x3|34|
| gmic  |fft 2048|57|
| cimg  |convolve 5x5|63.40|
| cimg  |convolve 3x3|29.98|
| cimg  |blur sigma=1|102.04|
| cimg  |blur sigma=3|100.40|
| cimg  |fft 2048|47.80|
| julia |convolve 5x5|494.0|
| julia |convolve 3x3|409.0|
| julia |fft 2048|241.0|
| julia gpu|convolve 5x5|27|
| julia gpu|fft 2048|80|
| julia af|convolve 5x5|5.0|
| julia af|fft 2048|13.0|


