using Images
using BenchmarkTools
using Statistics
using FFTW

t = @benchmark imfilter(img,h) setup=(img=randn(5000,5000); h=centered(ones(5,5));) samples = 50
print("* julia \tconvolve 5x5\t",round(1e-6*mean(t.times)),"\n")

t = @benchmark imfilter(img,h) setup=(img=randn(5000,5000);h=centered(ones(3,3));) samples = 50
print("* julia \tconvolve 3x3\t",round(1e-6*mean(t.times)),"\n")

t = @benchmark fft(img) setup=(img=randn(2048,2048)+randn(2048,2048)im;) samples = 50
print("* julia \tfft 2048\t",round(1e-6*mean(t.times)),"\n")

using GPUArrays
using CuArrays
function gpu_conv2(img,h)
    GPUArrays.convolution!(img,img,h);
    GPUArrays.synchronize(img);
end
try
    img = CuArray(randn(5000,5000));
    h = CuArray(ones(5,5));
    t = @benchmark gpu_conv2(img,h) samples = 50
    print("* julia gpu\tconvolve 5x5\t",round(1e-6*mean(t.times)),"\n")
catch
    print("* julia gpu\tconvolve 5x5\tfailed\n")
end

using CuArrays.CUFFT
function gpu_fft(img)
    fft(img)
    GPUArrays.synchronize(img);
end
try
    img = CuArray(randn(5000,5000));
    t = @benchmark gpu_fft(img) samples = 50
    print("* julia gpu\tfft 2048\t",round(1e-6*mean(t.times)),"\n")
catch
    print("* julia gpu\tfft 2048\tfailed\n")
end

using ArrayFire
t = @benchmark  Array(convolve2(img,h,UInt32(0),UInt32(0))) setup=(img=randn(AFArray{Float32},2048,2048);h=ones(AFArray{Float32},5,5);) samples = 50
print("* julia af\tconvolve 5x5\t",round(1e-6*mean(t.times)),"\n")

t = @benchmark Array(fft2(img)) setup=(img=randn(AFArray{Float32},2048,2048)+randn(AFArray{Float32},2048,2048)im;) samples = 50
print("* julia af\tfft 2048\t",round(1e-6*mean(t.times)),"\n")
