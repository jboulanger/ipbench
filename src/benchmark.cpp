#include "CImg.h"
using namespace cimg_library;

#define btime(K,fun,str) {double dt0 = 0; for (int k = 0; k < K; k++) {const unsigned long t0 = cimg::time(); fun; dt0 += (double)(cimg::time() - t0);}; printf("* cimg  \t%s\t%.2f\n",str,dt0/(double)K);}

int main(int argc, char*argv[] ) {
  cimg_usage("benchmark");
  const int K = 50;

  // convolve 5x5
  CImg<> img(5000,5000,1,1,0), h(5,5,1,1,1);
  img.noise(1);
  btime(K,img.correlate(h),"convolve 5x5");

  // convolve 3x3
  h.assign(3,3,1,1,1);
  btime(K,img.convolve(h),"convolve 3x3");

  // gaussian blur sigma 1
  btime(K,img.blur(1),"blur sigma=1");

  // gaussian blur sigma 3
  btime(K,img.blur(3),"blur sigma=3");

  // nd fft
  CImg<> re(2048,2048,1,1,0), im(2048,2048,1,1,0);
  re.noise(1);
  im.noise(1);
  btime(K,CImg<>::FFT(re,im),"fft 2048");


  return 0;
}
