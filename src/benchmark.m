% Image processing benchmark
%
% Jerome Boulanger 2019

K = 50; % number of run

if exist('OCTAVE_VERSION','builtin')
  pkg load image
  interpreter = 'octave';
else
  interpreter = 'matlab';
end


img = randn(5000,5000);
h = ones(5,5);
dt = zeros(1,K);
for i = 1:K
  tic;
  img = imfilter(img,h);
  dt(i) = toc;
end
fprintf('* %s\tconvolve 5x5\t%.2f\n', interpreter, mean(dt)*1000);

h = ones(3,3);
dt = zeros(1,K);
for i = 1:K
  tic;
  img = imfilter(img,h);
  dt(i) = toc;
end
fprintf('* %s\tconvolve 3x3\t%.2f\n', interpreter, mean(dt)*1000);

img = randn(2048,2048) + 1i*randn(N,N);
dt = zeros(1,K);
for i = 1:K
  tic;
  img = fft2(img);
  dt(i) = toc;
end

fprintf('* %s\tfft 2048\t%.2f\n', interpreter, mean(dt)*1000);
