#!/bin/bash
# Bash script running the same image processing benchmark
# using several programming environements
#
# Jerome Boulanger 2019

CPU=$(lscpu | grep "Model name" | sed 's/Model name: *//g')
NCPU=$(lscpu | grep -m 1 "CPU(s):" | sed 's/CPU(s): *\([0-9]*\)/\1/g')
GPU=$(lspci | grep VGA | sed 's/.*VGA.*: \(.*\)/\1/g')
echo "$CPU number of cores:$NCPU"
echo $GPU

if [ -f /usr/bin/matlab ]; then
    matlab -nodesktop -nodisplay -nosplash -nojvm -r benchmark,quit
fi

if [ -f /usr/bin/octave ]; then
    octave benchmark.m
fi

if [ -f /usr/bin/python3 ]; then
    python3 benchmark.py
fi

if [ -f /usr/bin/gmic ]; then
    gmic v - m benchmark.gmic benchmark
fi

if [ -f /usr/bin/g++ ]; then
    rm -f CImg.h;
    wget -q https://framagit.org/dtschump/CImg/raw/master/CImg.h
    g++ -Dcimg_use_openmp -Dcimg_use_fftw3 -Ofast  -I./ \
	benchmark.cpp -o benchmark.exe \
	-lfftw3 -lfftw3_threads -lm -fopenmp -pthread -lX11
    ./benchmark.exe
fi

if [ -f /usr/bin/julia ]; then
    julia benchmark.jl
fi
