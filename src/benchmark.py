from timeit import default_timer as timer
import numpy as np

L = 50
N = 5000
K = 5
a = np.random.randn(N,N)
h = np.ones([K, K])

# using scipy
from scipy import ndimage
dt = np.zeros(L)
for n in range(L):
    start = timer()
    ndimage.convolve(a, h, mode='reflect')
    end = timer()
    dt[n] = end - start
print("* scipy \tconvolve 5x5\t", round(dt.mean()*1000))

# using the PIL library
import PIL
img = PIL.Image.fromarray(a,'L') #cannot filter 32bit image??
k = PIL.ImageFilter.Kernel((5,5), h.flatten())
dt = np.zeros(L)
for n in range(L):
    start = timer()
    img.filter(k)
    end = timer()
    dt[n] = end - start
print("* pil   \tconvolve 5x5\t", round(dt.mean()*1000))

a = np.random.randn(2048,2048)
dt = np.zeros(L)
for n in range(L):
    start = timer()
    np.fft.fft2(a)
    end = timer()
    dt[n] = end - start
print("* numpy  \tfft 2048\t", round(dt.mean()*1000))
